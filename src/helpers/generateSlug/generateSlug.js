const generateSlug = (name) => {
  name = name.replace(/\s+/g, '-').toLowerCase()
  return name
}

export default generateSlug
